﻿namespace SDraw {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose ( bool disposing ) {
			if ( disposing && ( components != null ) ) {
				components . Dispose ( );
			}
			base . Dispose ( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ( ) {
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitF4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.brushToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fBrushToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.lineEraserNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.eraserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.undoZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.redoYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.panel1 = new System.Windows.Forms.Panel();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.menuStrip1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.brushToolStripMenuItem,
            this.fBrushToolStripMenuItem,
            this.lineEraserNToolStripMenuItem,
            this.eraserToolStripMenuItem,
            this.undoZToolStripMenuItem,
            this.redoYToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1236, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exitF4ToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
			this.saveToolStripMenuItem.Text = "Save";
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
			this.saveAsToolStripMenuItem.Text = "Save as";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// exportToolStripMenuItem
			// 
			this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
			this.exportToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
			this.exportToolStripMenuItem.Text = "Export";
			this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
			this.openToolStripMenuItem.Text = "Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// exitF4ToolStripMenuItem
			// 
			this.exitF4ToolStripMenuItem.Name = "exitF4ToolStripMenuItem";
			this.exitF4ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
			this.exitF4ToolStripMenuItem.Text = "Exit [!F4]";
			// 
			// brushToolStripMenuItem
			// 
			this.brushToolStripMenuItem.Name = "brushToolStripMenuItem";
			this.brushToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
			this.brushToolStripMenuItem.Text = "Main Brush [B]";
			// 
			// fBrushToolStripMenuItem
			// 
			this.fBrushToolStripMenuItem.Name = "fBrushToolStripMenuItem";
			this.fBrushToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
			this.fBrushToolStripMenuItem.Text = "Main FBrush [A]";
			// 
			// lineEraserNToolStripMenuItem
			// 
			this.lineEraserNToolStripMenuItem.Name = "lineEraserNToolStripMenuItem";
			this.lineEraserNToolStripMenuItem.Size = new System.Drawing.Size(113, 20);
			this.lineEraserNToolStripMenuItem.Text = "Alt Line Eraser [N]";
			// 
			// eraserToolStripMenuItem
			// 
			this.eraserToolStripMenuItem.Name = "eraserToolStripMenuItem";
			this.eraserToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
			this.eraserToolStripMenuItem.Text = "Alt Eraser [E]";
			this.eraserToolStripMenuItem.Click += new System.EventHandler(this.eraserToolStripMenuItem_Click);
			// 
			// undoZToolStripMenuItem
			// 
			this.undoZToolStripMenuItem.Name = "undoZToolStripMenuItem";
			this.undoZToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
			this.undoZToolStripMenuItem.Text = "Undo [^Z]";
			// 
			// redoYToolStripMenuItem
			// 
			this.redoYToolStripMenuItem.Name = "redoYToolStripMenuItem";
			this.redoYToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
			this.redoYToolStripMenuItem.Text = "Redo [^Y]";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Black;
			this.panel1.Controls.Add(this.textBox1);
			this.panel1.Cursor = System.Windows.Forms.Cursors.Cross;
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 24);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1236, 561);
			this.panel1.TabIndex = 1;
			this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
			this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
			this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(0, 0);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(365, 466);
			this.textBox1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1236, 585);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "SDraw Alpha";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System . Windows . Forms . MenuStrip menuStrip1;
		private System . Windows . Forms . ToolStripMenuItem fileToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem saveToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem saveAsToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem openToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem exitF4ToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem brushToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem lineEraserNToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem eraserToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem fBrushToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem undoZToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem redoYToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem exportToolStripMenuItem;
		private System . Windows . Forms . Panel panel1;
		private System . Windows . Forms . TextBox textBox1;
	}
}

