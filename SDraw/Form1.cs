﻿using System;
using System . Collections . Generic;
using System . Drawing;
using System . Windows . Forms;

namespace SDraw {
	public partial class Form1 : Form {
		public Form1 ( ) {
			InitializeComponent ( );
		} // ENDOF: CTOR

		/// <summary>
		/// Raised when Open dialog is requested. Either by user selecting a option or by God himself.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void openToolStripMenuItem_Click ( Object sender , EventArgs e ) {
			OpenFileDialog D = new OpenFileDialog(); // Open a file using system Open File Dialog.
			D . Filter = "SDraw Vector Image Format|*.SDV|Comma-Separated Values|*.CSV"; // Filte; CSV and SDV
			var u = D . ShowDialog ( ); // Show the dialog.
			if ( u == DialogResult . OK ) { // Check for answer.

				switch ( D.FileName.ToUpper() ) { // 🦀 IFs are gone 🦀

					case string x when x . Contains ( ".SDV" ):

						string[] v = System . IO . File . ReadAllLines ( D . FileName ); // Read the file and split it on line breaks.
						
						foreach ( var Y in v ) { // Parse each line.

							textBox1 . Text += Y + "\r\n"; // Rn its a temporary solution for debugging so dont look.

							if ( !(Y . Length < 4) ) {  // 🦀 fuck 🦀

								var m = Y . Split ( ',' ); // SYNTAX: LINESTART,LINEEND
								var n1 = m[0] . Split ( ';' ); // LINESTART
								var n2 = m[1] . Split ( ';' ); // LINEEND
								panel1 . CreateGraphics ( ) . DrawLine ( new Pen ( Color . White , 5 ) , new Point ( Convert . ToInt32 ( n1[0] ) , Convert . ToInt32 ( n1[1] ) ) , new Point ( Convert . ToInt32 ( n2[0] ) , Convert . ToInt32 ( n2[1] ) ) ); // Draw a line.

							} // ENDIF

						} // ENDOF: FOREACH

						break; // ENDOF: CASE:
					default:
						MessageBox . Show ( this , "Sorry, this format is not yet supported." , "Error" , MessageBoxButtons . OK , MessageBoxIcon . Error ); // Throw a ERRORBOX
						break; // ENDOF: DEFAULT:
				} // ENDOF: SWITCH ... CASE
			} // ENDIF
		} // ENDOF: OPEN EVENT
		/// <summary>
		/// Raised when SAVE dialog is requested by whatever.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void saveAsToolStripMenuItem_Click ( Object sender , EventArgs e ) {
			SaveFileDialog D = new SaveFileDialog(); // System SAVE dialog.
			D . Filter = "SDraw Vector Image Format|*.SDV|Comma-Separated Values|*.CSV|All Files|*.*"; // Filter
			// In theory, save dialog should only output files that can be read by the program that saved them.
			// I'm sticking to this here.
			var u = D . ShowDialog ( ); // Show the save dialog.
			if ( u == DialogResult . OK ) { // Check for result.
				switch ( D . FileName . ToUpper ( ) ) { // IFs bad.
					case string x when x . Contains ( ".SDV" ): // Default format so...
						System . IO . File . WriteAllText ( D . FileName , textBox1 . Text ); // ...we don't need to do anything fancy here.
						break; // ENDOF: CASE:
					default:
						MessageBox . Show ( this , "Sorry, this format is not yet supported." , "Error" , MessageBoxButtons . OK , MessageBoxIcon . Error ); // Throw a ERRORBOX copied straight from OPEN event.
						break; // ENDOF: DEFAULT:
				} // ENDOF: SWITCH ... CASE
			} // ENDIF
		} // ENDOF: SAVE EVENT

		/// <summary>
		/// Raised when EXPORT dialog is requested.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void exportToolStripMenuItem_Click ( Object sender , EventArgs e ) {
			SaveFileDialog D = new SaveFileDialog(); // TODO: Replace SAVE with EXPORT and give user in-dialog options.
			D . Filter = "Scalable Vector Graphics|*.SVG|Bitmap|*.BMP|Portable Network Graphics|*.PNG|Bare Pixel Array|*.ASM|C# Code|*.CS"; // Filter. Both vector and raster formats live in harmony here.
			var u = D . ShowDialog ( ); // Show the dialog.

			if ( u == DialogResult . OK ) { // And check if EXPORT was selected.

				switch ( D . FileName . ToUpper ( ) ) { // IFs bad.
					case string x when x . Contains ( ".SVG" ): // Might be good to have.
						string RFl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<svg xmlns = \"http://www.w3.org/2000/svg\" width = \"" + this . Width + "\" height = \"" + this . Height + "\"> \r\n<rect width = \"" + this . Width + "\" height = \"" + this . Height + "\" fill=\"black\"/>\r\n>"; // SVG shit
						var v = textBox1 . Text . Split ( '\n' ); // Split on LF.
						foreach ( var Y in v ) { // Friendly FOREACH for parsing the line.
							switch ( Y.Length ) { // Stacked SWITCH ... CASE statements are the way to go.
								case int Z when Z > 4: // Each line is signed for UNDO and REDO, we have to skip the signatures somehow.
									var m = Y . Split ( ',' ); // Again, LINESTART, LINEEND
									var n1 = m[0] . Split ( ';' ); // LINESTART
									var n2 = m[1] . Split ( ';' ); // LINEEND
									try { // Just don't look.
										RFl += "<line x1=\"" + n1[0] + "\" y1=\"" + n1[1] + "\" x2=\"" + n2[0] + "\" y2=\"" + n2[1] . Replace ( "\r" , "" ) . Replace ( "\n" , "" ) + "\" style=\"stroke:rgb( 255,255,255 );stroke-width:5\"/>\r\n"; // Append <line/> thingy.
									} catch { } // OK, you can start looking again.
									break; // ENDOF: CASE:
								default:
									break; // ENDOF: DEFAULT:
							} // ENDOF: SWITCH ... CASE (STACKED)
						} // ENDOF: FOREACH
						RFl += "</svg>"; // End the SVG
						System . IO . File . WriteAllText ( D . FileName , RFl ); // And write it
						break; // ENDOF: CASE: SVG
					default:
						MessageBox . Show ( this , "Sorry, this format is not yet supported." , "Error" , MessageBoxButtons . OK , MessageBoxIcon . Error ); // Hmmm
						break; // ENDOF: DEFAULT:
				} // ENDOF: SWITCH ... CASE
			} // ENFIF

		} // ENDOF: EXPORT EVENT

		private void panel1_MouseDown ( Object sender , MouseEventArgs e ) {
			if ( G == null ) // Graphics ofc shouldn't be NULL.
				panel1 . CreateGraphics ( ); // So handle it by using the built-in sub.
			textBox1 . Text = "\r\n" + LC + "S"+ textBox1 . Text; // Append a GROUP
			P1 = e . Location; // Start point.
			IsMouseDown = true; // Set IsMouseDown to TRUE.
		}
		private void eraserToolStripMenuItem_Click ( Object sender , EventArgs e ) {
			
		}
		/// <summary>
		/// Handle the MOUSEMOVE event. To be honest, I have no idea what's going on here, but the result is so interesting I decided to leave the function unchanged.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void panel1_MouseMove ( Object sender , MouseEventArgs e ) {
			if ( IsMouseDown ) {// Check if mouse button is down.
				switch ( Pt2 ) { // Check for god knows what.
					case true:
						panel1 . CreateGraphics ( ) . DrawLine ( new Pen ( Color . White , 5 ) , P1 , P2 ); // Draw (something)
						var c = new KeyValuePair<Point , Point> ( P1 , P2 ); // This IS a bad idea. TODO: Tuple<Int32,Int32>
						LLine . Add ( c ); // Add a line to the "list".
						textBox1 . Text = "\r\n" + P1 . X + ";" + P1 . Y + "," + P2 . X + ";" + P2 . Y + textBox1 . Text; // Write the line.
						P1 = e . Location; // Something to do with mouse pointer location and initial line position.
						Pt2 = false; // What?
						break; // ENDOF: TRUE
					case false:
						P2 = e . Location; // Something to do with mouse pointer location and end line position.
						Pt2 = true; // Endpoint?
						break; // ENDOF: FALSE
				} // ENDOF: SWITCH ... CASE
			} // ENDIF
		} // ENDOF: MOUSEMOVE EVENT

		private void panel1_MouseUp ( Object sender , MouseEventArgs e ) {
			Pt2 = false; // Endpoint or something.
			IsMouseDown = false; // Mouse is no longer down.
			textBox1 . Text = "\r\n" + LC + "E" + textBox1 . Text; // END: GROUP
			LC++; // Add 1 to GROUP counter.
		} // ENDOF: MOUSEUP EVENT

		#region PRIVATE VARIABLES
		/// <summary>
		/// Maing graphics variable.
		/// </summary>
		Graphics G = null;
		/// <summary>
		/// Line counter.
		/// </summary>
		UInt64 LC = 0;
		/// <summary>
		/// Is the mouse down?
		/// </summary>
		bool IsMouseDown = false;
		/// <summary>
		/// Something.
		/// </summary>
		bool Pt2 = false;
		/// <summary>
		/// LINEEND
		/// </summary>
		Point P2 = new Point ( );
		/// <summary>
		/// LINESTART
		/// </summary>
		Point P1 = new Point ( );
		/// <summary>
		/// Lines list.
		/// </summary>
		List<KeyValuePair<Point , Point>> LLine = new List<KeyValuePair<Point , Point>> ( );
		#endregion

	} // ENDOF: CLASS Form1
} // ENDOF: NAMESPACE
